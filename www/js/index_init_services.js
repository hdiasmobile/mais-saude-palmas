/* --------------
 initialization 
  the xdkFilter argument can be set to a function that
   receives the data of the service method and can return alternate data
   thus you can reformat dates or names, remove or add entries, etc.
   -------------- */


data_support.ready(intel.xdk.services.postos_de_saude.bind(null, {"xdkFilter":null}));
data_support.ready(intel.xdk.services.especialidades.bind(null, {"xdkFilter":null}));
data_support.ready(intel.xdk.services.emergencia.bind(null, {"xdkFilter":null}));